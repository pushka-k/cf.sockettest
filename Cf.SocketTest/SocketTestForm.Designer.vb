﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SocketTestForm
	Inherits System.Windows.Forms.Form

	'Форма переопределяет dispose для очистки списка компонентов.
	<System.Diagnostics.DebuggerNonUserCode()> _
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Является обязательной для конструктора форм Windows Forms
	Private components As System.ComponentModel.IContainer

	'Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
	'Для ее изменения используйте конструктор форм Windows Form.  
	'Не изменяйте ее в редакторе исходного кода.
	<System.Diagnostics.DebuggerStepThrough()> _
	Private Sub InitializeComponent()
		Me._txtIp = New System.Windows.Forms.TextBox()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.Label2 = New System.Windows.Forms.Label()
		Me._txtPort = New System.Windows.Forms.TextBox()
		Me._btnFirmwareUpload = New System.Windows.Forms.Button()
		Me._txtFirmUplRes = New System.Windows.Forms.Label()
		Me.TabControl1 = New System.Windows.Forms.TabControl()
		Me.TabPage1 = New System.Windows.Forms.TabPage()
		Me.TabPage2 = New System.Windows.Forms.TabPage()
		Me._btnProcessFile = New System.Windows.Forms.Button()
		Me.Label4 = New System.Windows.Forms.Label()
		Me._txtReceivedBytes = New System.Windows.Forms.TextBox()
		Me.Label3 = New System.Windows.Forms.Label()
		Me._txtSendBytes = New System.Windows.Forms.TextBox()
		Me._btnSend = New System.Windows.Forms.Button()
		Me.TabControl1.SuspendLayout()
		Me.TabPage1.SuspendLayout()
		Me.TabPage2.SuspendLayout()
		Me.SuspendLayout()
		'
		'_txtIp
		'
		Me._txtIp.Location = New System.Drawing.Point(12, 25)
		Me._txtIp.Name = "_txtIp"
		Me._txtIp.Size = New System.Drawing.Size(129, 20)
		Me._txtIp.TabIndex = 2
		Me._txtIp.Text = "55.255.0.100"
		'
		'Label1
		'
		Me.Label1.AutoSize = True
		Me.Label1.Location = New System.Drawing.Point(12, 9)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(50, 13)
		Me.Label1.TabIndex = 3
		Me.Label1.Text = "IP адрес"
		'
		'Label2
		'
		Me.Label2.AutoSize = True
		Me.Label2.Location = New System.Drawing.Point(162, 28)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(32, 13)
		Me.Label2.TabIndex = 5
		Me.Label2.Text = "Порт"
		'
		'_txtPort
		'
		Me._txtPort.Location = New System.Drawing.Point(200, 25)
		Me._txtPort.Name = "_txtPort"
		Me._txtPort.Size = New System.Drawing.Size(84, 20)
		Me._txtPort.TabIndex = 4
		Me._txtPort.Text = "90"
		'
		'_btnFirmwareUpload
		'
		Me._btnFirmwareUpload.Location = New System.Drawing.Point(6, 6)
		Me._btnFirmwareUpload.Name = "_btnFirmwareUpload"
		Me._btnFirmwareUpload.Size = New System.Drawing.Size(129, 23)
		Me._btnFirmwareUpload.TabIndex = 10
		Me._btnFirmwareUpload.Text = "Обновить прошивку"
		Me._btnFirmwareUpload.UseVisualStyleBackColor = True
		'
		'_txtFirmUplRes
		'
		Me._txtFirmUplRes.AutoSize = True
		Me._txtFirmUplRes.Location = New System.Drawing.Point(6, 32)
		Me._txtFirmUplRes.Name = "_txtFirmUplRes"
		Me._txtFirmUplRes.Size = New System.Drawing.Size(10, 13)
		Me._txtFirmUplRes.TabIndex = 14
		Me._txtFirmUplRes.Text = "-"
		'
		'TabControl1
		'
		Me.TabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
			Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.TabControl1.Controls.Add(Me.TabPage1)
		Me.TabControl1.Controls.Add(Me.TabPage2)
		Me.TabControl1.Location = New System.Drawing.Point(12, 51)
		Me.TabControl1.Name = "TabControl1"
		Me.TabControl1.SelectedIndex = 0
		Me.TabControl1.Size = New System.Drawing.Size(324, 335)
		Me.TabControl1.TabIndex = 15
		'
		'TabPage1
		'
		Me.TabPage1.BackColor = System.Drawing.SystemColors.Control
		Me.TabPage1.Controls.Add(Me._btnFirmwareUpload)
		Me.TabPage1.Controls.Add(Me._txtFirmUplRes)
		Me.TabPage1.Location = New System.Drawing.Point(4, 22)
		Me.TabPage1.Name = "TabPage1"
		Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
		Me.TabPage1.Size = New System.Drawing.Size(316, 309)
		Me.TabPage1.TabIndex = 0
		Me.TabPage1.Text = "Обновление прошивки"
		'
		'TabPage2
		'
		Me.TabPage2.BackColor = System.Drawing.SystemColors.Control
		Me.TabPage2.Controls.Add(Me._btnProcessFile)
		Me.TabPage2.Controls.Add(Me.Label4)
		Me.TabPage2.Controls.Add(Me._txtReceivedBytes)
		Me.TabPage2.Controls.Add(Me.Label3)
		Me.TabPage2.Controls.Add(Me._txtSendBytes)
		Me.TabPage2.Controls.Add(Me._btnSend)
		Me.TabPage2.Location = New System.Drawing.Point(4, 22)
		Me.TabPage2.Name = "TabPage2"
		Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
		Me.TabPage2.Size = New System.Drawing.Size(316, 309)
		Me.TabPage2.TabIndex = 1
		Me.TabPage2.Text = "Выполнение команд"
		'
		'_btnProcessFile
		'
		Me._btnProcessFile.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me._btnProcessFile.Location = New System.Drawing.Point(3, 262)
		Me._btnProcessFile.Name = "_btnProcessFile"
		Me._btnProcessFile.Size = New System.Drawing.Size(307, 41)
		Me._btnProcessFile.TabIndex = 15
		Me._btnProcessFile.Text = "Обработать команды из файла"
		Me._btnProcessFile.UseVisualStyleBackColor = True
		'
		'Label4
		'
		Me.Label4.AutoSize = True
		Me.Label4.Location = New System.Drawing.Point(3, 171)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(91, 13)
		Me.Label4.TabIndex = 14
		Me.Label4.Text = "Ответные байты"
		'
		'_txtReceivedBytes
		'
		Me._txtReceivedBytes.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
			Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me._txtReceivedBytes.Location = New System.Drawing.Point(3, 187)
		Me._txtReceivedBytes.Multiline = True
		Me._txtReceivedBytes.Name = "_txtReceivedBytes"
		Me._txtReceivedBytes.ScrollBars = System.Windows.Forms.ScrollBars.Both
		Me._txtReceivedBytes.Size = New System.Drawing.Size(307, 69)
		Me._txtReceivedBytes.TabIndex = 13
		'
		'Label3
		'
		Me.Label3.AutoSize = True
		Me.Label3.Location = New System.Drawing.Point(3, 3)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(110, 13)
		Me.Label3.TabIndex = 12
		Me.Label3.Text = "Байты для отправки"
		'
		'_txtSendBytes
		'
		Me._txtSendBytes.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me._txtSendBytes.Location = New System.Drawing.Point(3, 19)
		Me._txtSendBytes.Multiline = True
		Me._txtSendBytes.Name = "_txtSendBytes"
		Me._txtSendBytes.ScrollBars = System.Windows.Forms.ScrollBars.Both
		Me._txtSendBytes.Size = New System.Drawing.Size(307, 109)
		Me._txtSendBytes.TabIndex = 11
		Me._txtSendBytes.Text = "00 00 06 01 ff ff"
		'
		'_btnSend
		'
		Me._btnSend.Location = New System.Drawing.Point(3, 134)
		Me._btnSend.Name = "_btnSend"
		Me._btnSend.Size = New System.Drawing.Size(75, 23)
		Me._btnSend.TabIndex = 10
		Me._btnSend.Text = "Send"
		Me._btnSend.UseVisualStyleBackColor = True
		'
		'SocketTestForm
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(348, 398)
		Me.Controls.Add(Me.TabControl1)
		Me.Controls.Add(Me.Label2)
		Me.Controls.Add(Me._txtPort)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me._txtIp)
		Me.Name = "SocketTestForm"
		Me.Text = "Тритон - Утилита для зарузки"
		Me.TabControl1.ResumeLayout(False)
		Me.TabPage1.ResumeLayout(False)
		Me.TabPage1.PerformLayout()
		Me.TabPage2.ResumeLayout(False)
		Me.TabPage2.PerformLayout()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub
	Friend WithEvents _txtIp As System.Windows.Forms.TextBox
	Friend WithEvents Label1 As System.Windows.Forms.Label
	Friend WithEvents Label2 As System.Windows.Forms.Label
	Friend WithEvents _txtPort As System.Windows.Forms.TextBox
	Friend WithEvents _btnFirmwareUpload As System.Windows.Forms.Button
	Friend WithEvents _txtFirmUplRes As System.Windows.Forms.Label
	Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
	Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
	Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
	Friend WithEvents _btnProcessFile As System.Windows.Forms.Button
	Friend WithEvents Label4 As System.Windows.Forms.Label
	Friend WithEvents _txtReceivedBytes As System.Windows.Forms.TextBox
	Friend WithEvents Label3 As System.Windows.Forms.Label
	Friend WithEvents _txtSendBytes As System.Windows.Forms.TextBox
	Friend WithEvents _btnSend As System.Windows.Forms.Button

End Class
