﻿Imports System.Net.Sockets
Imports System.Net

Public Class Utils
	Public Shared Function BytesToString(bytes As Byte()) As String
		Dim result = ""
		For Each b In bytes
			If result <> "" Then
				result += " "
			End If
			result += Hex(b).PadLeft(2, "0"c)
		Next
		Return result
	End Function

	Public Shared Function StringToBytes(str As String) As Byte()
		Dim bytes As New List(Of Byte)
		Dim correct As Boolean = True
		For i = 0 To str.Length - 1 Step 3
			Dim part = Mid(str, i + 1, 3)
			Try
				Dim b = CByte(Val("&H" + part))
				bytes.Add(b)
			Catch ex As Exception
				Throw New Exception("Bad String Format, not sequense of hex")
			End Try
		Next
		Return bytes.ToArray
	End Function

	Public Shared Function SendMessageFromSocket(ip As String, port As Integer, sendBytes As Byte(), offset As Integer, size As Integer, Optional timeOut As Integer = 500) As Byte()
		' Устанавливаем удаленную точку для сокета
		'Dim ipHost As IPHostEntry = New 
		'Dim ipHost As IPHostEntry = Dns.GetHostEntry(ip)
		'Dim ipAddr As IPAddress = ipHost.AddressList(0)

		Dim ipEndPoint As New IPEndPoint(IPAddress.Parse(ip), port)

		Dim sender As New Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)
		sender.SendTimeout = timeOut
		sender.ReceiveTimeout = timeOut
		' Соединяем сокет с удаленной точкой
		sender.Connect(ipEndPoint)

		' Отправляем данные через сокет
		Dim bytesSent As Integer = sender.Send(sendBytes, offset, size, SocketFlags.None)
		' Буфер для входящих данных
		Dim bytes As Byte() = New Byte(10230) {}
		' Получаем ответ от сервера
		Dim bytesRec As Integer = sender.Receive(bytes)
		ReDim Preserve bytes(bytesRec - 1)
		' Освобождаем сокет
		sender.Shutdown(SocketShutdown.Both)
		sender.Close()
		sender.Dispose()

		Return bytes
	End Function
End Class
