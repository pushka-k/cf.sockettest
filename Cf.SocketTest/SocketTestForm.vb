﻿Imports System.Net.Sockets
Imports System.Net
Imports System.Text
Imports System.IO

Public Class SocketTestForm

	Private Sub _btnSend_Click(sender As Object, e As EventArgs) Handles _btnSend.Click
		Try
			Dim msg = Utils.StringToBytes(_txtSendBytes.Text)
			Dim res = Utils.SendMessageFromSocket(_txtIp.Text, Convert.ToInt32(_txtPort.Text), msg, 0, msg.Length)
			_txtReceivedBytes.Text = Utils.BytesToString(res)
		Catch ex As Exception
			MessageBox.Show(ex.ToString)
		End Try
	End Sub

	Private Sub _btnProcessFile_Click(sender As Object, e As EventArgs) Handles _btnProcessFile.Click
		Using f As New OpenFileDialog
			f.DefaultExt = "*.txt"
			f.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory
			If f.ShowDialog = Windows.Forms.DialogResult.OK Then
				Dim form = New ProcessFileForm(f.FileName)
				form.Show()
			End If
		End Using
	End Sub

	Private Sub UplFirm(fName As String)
		Try
			Dim msg = Utils.StringToBytes("00 00 00 FF")
			Utils.SendMessageFromSocket(_txtIp.Text, Convert.ToInt32(_txtPort.Text), msg, 0, msg.Length)

			Dim f = File.OpenRead(fName)
			Dim bytesSend = 0
			Dim length = f.Length
			Try
				Dim bytes(128) As Byte
				Dim bytesCount = 0
				Dim timeOut = 1000
				While True
					bytesCount = f.Read(bytes, 0, bytes.Length)
					If bytesCount = 0 Then
						Exit While
					End If
					Dim res = Utils.SendMessageFromSocket(_txtIp.Text, Convert.ToInt32(_txtPort.Text), bytes, 0, bytesCount, timeOut)
					If (res Is Nothing) OrElse (Not res.Any) OrElse (res(0) = 0) Then
						Dim resStr = "-"
						If res IsNot Nothing AndAlso res.Any Then
							resStr = Utils.BytesToString(res)
						End If
						If res IsNot Nothing Then
							Throw New Exception("Получен плохой ответ контроллера: " + resStr)
						End If
					End If
					bytesSend += bytesCount
					Dim s = Sub()
								_txtFirmUplRes.Text = "Отправлено " + bytesSend.ToString + " из " + length.ToString
							End Sub
					BeginInvoke(s)
					Array.Clear(bytes, 0, bytes.Length)
				End While
			Finally
				Try
					f.Dispose()
				Catch ex As Exception
				End Try
			End Try
		Catch ex As Exception
			Dim s = Sub()
						_txtFirmUplRes.Text += " ОШИБКА"
					End Sub
			BeginInvoke(s)
			MessageBox.Show(ex.ToString, "Ошибка обновления прошивки")
		End Try
	End Sub

	Private Sub _btnFirmwareUpload_Click(sender As Object, e As EventArgs) Handles _btnFirmwareUpload.Click
		Try
			Using dlg As New OpenFileDialog()
				dlg.DefaultExt = "*.bin"
				dlg.Title = "Выберите бинарный файл с прошивкой"
				dlg.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory
				If dlg.ShowDialog = Windows.Forms.DialogResult.OK Then
					_txtFirmUplRes.Text = "Начато обновление прошивки"
					Dim f = Sub()
								UplFirm(dlg.FileName)
							End Sub
					Dim t = New Threading.Thread(f)
					t.Start()
				End If
			End Using
		Catch ex As Exception
			MessageBox.Show(ex.ToString, "Ошибка обновления прошивки")
		End Try
	End Sub
End Class
