﻿Imports System.IO

Public Class ProcessFileForm
	Private ReadOnly _lines As New List(Of String)
	Private ReadOnly _fName As String

	Public Sub New(fName As String)

		' Этот вызов является обязательным для конструктора.
		InitializeComponent()

		' Добавьте все инициализирующие действия после вызова InitializeComponent().
		_fName = fName
		Dim t = New Threading.Thread(AddressOf ProcessFileThread)
		t.SetApartmentState(Threading.ApartmentState.STA)
		t.Start()
	End Sub

	Private Sub _btnClose_Click(sender As Object, e As EventArgs) Handles _btnClose.Click
		Close()
	End Sub

	Private Sub ShowStr(Optional str As String = "")
		Dim f = Sub()
					_lines.Add(str)
					_txtOutput.Lines = _lines.ToArray
				End Sub
		If InvokeRequired Then
			BeginInvoke(f)
		Else
			f()
		End If
	End Sub

	Private Sub ProcessFileThread()
		Try
			ShowStr("Начало обработки файла:")
			ShowStr(_fName)
			ShowStr()
			Dim fName = _fName
			Dim fileStrings = File.ReadAllLines(fName)
			If fileStrings IsNot Nothing AndAlso fileStrings.Any Then
				Dim ip = fileStrings(0)
				Dim port = Convert.ToInt32(fileStrings(1))
				ShowStr("IP: " + ip)
				ShowStr("Порт: " + port.ToString)
				ShowStr()
				For i = 2 To fileStrings.Length - 1
					If String.IsNullOrEmpty(fileStrings(i)) Then
						Continue For
					End If

					If fileStrings(i).ToLower.Replace("'", "/").Replace("\", "/")(0) = "/" Then
						ShowStr(fileStrings(i))
						Continue For
					End If

					If fileStrings(i).ToLower.Contains("sleep") Then
						Try
							Dim t = Convert.ToInt32(fileStrings(i).Replace("_", " ").Split(" "c)(1))
							ShowStr("Ожидание " + t.ToString + " милисек")
							ShowStr()
							Threading.Thread.Sleep(t)
						Catch ex As Exception
							ShowStr("Ошибка ожидания: " + ex.ToString)
						End Try
						Continue For
					End If

					Dim senCmd = fileStrings(i)
					Dim sendCmdBytes = Utils.StringToBytes(senCmd)
					i += 1
					Dim answerWanted = fileStrings(i)

					ShowStr("Отправляется команда: ")
					ShowStr(senCmd)
					Dim answerRealBytes = Utils.SendMessageFromSocket(ip, port, sendCmdBytes, 0, sendCmdBytes.Length)
					Dim answerReal = Utils.BytesToString(answerRealBytes).ToUpper
					ShowStr("Получен ответ: ")
					ShowStr(answerReal)
					ShowStr("Ожидался ответ: ")
					ShowStr(answerWanted)
					ShowStr()
					Dim goodAnswerStr = "--- ОШИБКА: ПЛОХОЙ ОТВЕТ НА КОМАНДУ"
					If answerReal = answerWanted Then
						goodAnswerStr = "+++ ОТВЕТ ВЕРНЫЙ"
					End If
					ShowStr(goodAnswerStr)
					ShowStr()
					ShowStr()
				Next
			End If
		Catch ex As Exception
			ShowStr()
			ShowStr("Ошибка обработки файла:")
			ShowStr(ex.ToString)
			ShowStr()
		End Try
	End Sub
End Class