﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ProcessFileForm
    Inherits System.Windows.Forms.Form

    'Форма переопределяет dispose для очистки списка компонентов.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Является обязательной для конструктора форм Windows Forms
    Private components As System.ComponentModel.IContainer

    'Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
    'Для ее изменения используйте конструктор форм Windows Form.  
    'Не изменяйте ее в редакторе исходного кода.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Me._btnClose = New System.Windows.Forms.Button()
		Me._txtOutput = New System.Windows.Forms.TextBox()
		Me.SuspendLayout()
		'
		'_btnClose
		'
		Me._btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me._btnClose.Location = New System.Drawing.Point(461, 448)
		Me._btnClose.Name = "_btnClose"
		Me._btnClose.Size = New System.Drawing.Size(128, 38)
		Me._btnClose.TabIndex = 1
		Me._btnClose.Text = "Закрыть"
		Me._btnClose.UseVisualStyleBackColor = True
		'
		'_txtOutput
		'
		Me._txtOutput.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
			Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me._txtOutput.BackColor = System.Drawing.SystemColors.HighlightText
		Me._txtOutput.Location = New System.Drawing.Point(12, 12)
		Me._txtOutput.Multiline = True
		Me._txtOutput.Name = "_txtOutput"
		Me._txtOutput.ReadOnly = True
		Me._txtOutput.ScrollBars = System.Windows.Forms.ScrollBars.Both
		Me._txtOutput.Size = New System.Drawing.Size(577, 430)
		Me._txtOutput.TabIndex = 2
		'
		'ProcessFileForm
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(601, 498)
		Me.Controls.Add(Me._txtOutput)
		Me.Controls.Add(Me._btnClose)
		Me.Name = "ProcessFileForm"
		Me.Text = "Обработка файла с командами"
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub
	Friend WithEvents _btnClose As System.Windows.Forms.Button
	Friend WithEvents _txtOutput As System.Windows.Forms.TextBox
End Class
